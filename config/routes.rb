Rails.application.routes.draw do
  
  resources :projects
  resources :roles
  resources :cells
  get 'directories/index'

  get 'directories/show'

  devise_for :users

  resources :questions do
    member do
      put "like", to: "questions#upvote"
      put "dislike", to: "questions#downvote"
    end
  end
  
  resources :posts do
    member do
      put "like", to: "posts#upvote"
      put "dislike", to: "posts#downvote"
    end
  end


  get '/home', to: "home#index"
  root :to => "home#index"
  

  post "/post_comment/", to: "post_comments#create", as: :post_comments
  post "/question_comment/", to: "question_comments#create", as: :question_comments

  #diretorias
  get "/directories", to: "directories#index", as: :directories
  get "/directory/:id", to: "directories#show", as: :directory
  get "directories/new", to: "directories#new", as: :new_directory
  post "directories/", to: "directories#create", as: :create_directory
  delete "directory/:id", to: "directories#destroy", as: :remove_directory


  post "/enter_directory", to: "directories#enter_directory", as: :enter_directory
  post "/fusion_ask", to: "directories#fusion_ask", as: :fusion_ask
  get "/fusion_ask_requests", to: "directories#fusion_ask_request", as: :fusion_ask_request
  post "/fusion_ask_accept", to: "directories#fusion_ask_accept", as: :fusion_ask_accept
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  #cell
  post "/participate_cell", to: "cells#participate_cell", as: :participate_cell

  #projects
  post "/enter_project", to: "projects#enter_project", as: :enter_project
  post "/accept_request", to: "projects#accept_project_requests", as: :accept_project_requests
end
