class ProjectsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_project, only: [:show, :edit, :update, :destroy]
  

  # GET /projects
  # GET /projects.json
  def index
    @projects = Project.all
    @user = current_user
  end

  # GET /projects/1
  # GET /projects/1.json
  def show
    
    @user_projects = @project.user_projects
    @members = @project.users
    @user = current_user
    @projects = @user.projects
    @has_project = false

    @projects.each do |p|
      if p == @project 
        @has_project = true
      end
    end
  end

  # GET /projects/new
  def new
    @user = current_user
    @project = Project.new
    
  end

  # GET /projects/1/edit
  def edit
  end

  # POST /projects
  # POST /projects.json
  def create
    @project = Project.new(project_params)

    respond_to do |format|
      if @project.save
        format.html { redirect_to @project, notice: 'Project was successfully created.' }
        format.json { render :show, status: :created, location: @project }
      else
        format.html { render :new }
        format.json { render json: @project.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /projects/1
  # PATCH/PUT /projects/1.json
  def update
    respond_to do |format|
      if @project.update(project_params)
        format.html { redirect_to @project, notice: 'Project was successfully updated.' }
        format.json { render :show, status: :ok, location: @project }
      else
        format.html { render :edit }
        format.json { render json: @project.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /projects/1
  # DELETE /projects/1.json
  def destroy
    @project.destroy
    respond_to do |format|
      format.html { redirect_to projects_url, notice: 'Project was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def enter_project
    
    @user_project = UserProject.create(project_id:params[:project_id], user_id:params[:user_id], status:false)
    redirect_to project_path(params[:project_id])
  end

  def accept_project_requests
    @user_project = UserProject.find(params[:user_project_id])
    @user_project.status = true
    @user_project.save
    redirect_to project_path(@project.id)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_project
      @project = Project.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def project_params
      params.require(:project).permit(:name, :description, :tecnologies, :begining_date, :expected_end_date, :end_date, :manager)
    end
end
