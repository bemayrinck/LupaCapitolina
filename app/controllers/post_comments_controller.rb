class PostCommentsController < ApplicationController
    before_action :authenticate_user!
    
    def create
       # @post = Post.find('1')
        @comment = PostComment.create(params[:post_comment].permit(:body,:user_id,:post_id))
        @post = Post.find(params[:post_comment][:post_id])
        redirect_to post_path(@post)
    end
end
