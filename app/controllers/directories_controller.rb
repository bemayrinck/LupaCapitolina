class DirectoriesController < ApplicationController
  before_action :authenticate_user!

  def index
    @directories = Directory.all
  end

  def new
    @directory = Directory.new
  end

  def create
    @directory = Directory.create(params[:directory].permit(:name,:description))
    redirect_to directories_path
  end

  def show
    @user = current_user
    @directory = Directory.find(params[:id])
    @users = @directory.users.all
    @fusion_asks = @directory.fusion_asks
  end

  def enter_directory
    @user = current_user
    @directory = Directory.find(params[:directory_id])
    @user.directory_id = @directory.id
    @user.save

    redirect_to directory_path(@directory)
  end

  def fusion_ask
    
    fusion_ask = FusionAsk.create(user_id:params[:user_id], directory_id:params[:directory_id], status:false)
    @directory = Directory.find(params[:directory_id])
    redirect_to directory_path(@directory)
  end

  def fusion_ask_request
    @fusion_asks = FusionAsk.all
  end

  def fusion_ask_accept
    @fusion_asks = FusionAsk.where(user_id:params[:user_id], directory_id:params[:directory_id])
    @fusion_asks.each do |f|
      f.status = true
      f.save
    end

    redirect_to fusion_ask_request_path
    
  end

  def destroy
    @directory = Directory.find(params[:id])
    @directory.destroy
    redirect_to directories_path
    
  end




end
