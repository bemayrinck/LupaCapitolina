class QuestionCommentsController < ApplicationController
    before_action :authenticate_user!
    def create
        
        @comment = QuestionComment.create(params[:question_comment].permit(:body,:user_id,:question_id))
        @question = Question.find(params[:question_comment][:question_id])
        redirect_to question_path(@question)
        
     end
end
