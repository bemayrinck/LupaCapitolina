json.extract! project, :id, :name, :description, :tecnologies, :begining_date, :expected_end_date, :end_date, :manager, :created_at, :updated_at
json.url project_url(project, format: :json)
