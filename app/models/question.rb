class Question < ApplicationRecord
  belongs_to :user
  has_many :question_comments, dependent: :delete_all
  acts_as_votable
end
