class Post < ApplicationRecord
  belongs_to :user
  has_many :post_comments, dependent: :delete_all
  acts_as_votable
end
