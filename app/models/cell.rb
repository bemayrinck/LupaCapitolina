class Cell < ApplicationRecord
    has_many :users

    has_many :user_cells
    has_many :users, through: :user_cells
    
end
