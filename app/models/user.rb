class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :posts

  has_many :questions

  has_many :post_comments

  belongs_to :directory, optional: true
  has_many :fusion_asks
  
  has_many :user_cells, dependent: :delete_all
  has_many :cells, through: :user_cells
  
  has_many :user_projects, dependent: :delete_all
  has_many :projects, through: :user_projects


  acts_as_voter

end
