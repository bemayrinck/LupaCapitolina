require 'test_helper'

class DirectoriesControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get directories_index_url
    assert_response :success
  end

  test "should get show" do
    get directories_show_url
    assert_response :success
  end

end
