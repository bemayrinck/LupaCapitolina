class CreateUserCells < ActiveRecord::Migration[5.1]
  def change
    create_table :user_cells do |t|
      t.references :user, foreign_key: true
      t.references :cell, foreign_key: true

      t.timestamps
    end
  end
end
