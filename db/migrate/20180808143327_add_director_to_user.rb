class AddDirectorToUser < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :director, :boolean
  end
end
