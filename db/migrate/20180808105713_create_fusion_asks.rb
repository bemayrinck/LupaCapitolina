class CreateFusionAsks < ActiveRecord::Migration[5.1]
  def change
    create_table :fusion_asks do |t|
      t.references :user, foreign_key: true
      t.references :directory, foreign_key: true

      t.timestamps
    end
  end
end
