class CreateProjects < ActiveRecord::Migration[5.1]
  def change
    create_table :projects do |t|
      t.string :name
      t.text :description
      t.text :tecnologies
      t.datetime :begining_date
      t.datetime :expected_end_date
      t.datetime :end_date
      t.string :manager

      t.timestamps
    end
  end
end
