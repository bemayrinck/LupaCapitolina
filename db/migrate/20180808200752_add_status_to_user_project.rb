class AddStatusToUserProject < ActiveRecord::Migration[5.1]
  def change
    add_column :user_projects, :status, :boolean
  end
end
