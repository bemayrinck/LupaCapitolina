class AddStatusToFusionAsk < ActiveRecord::Migration[5.1]
  def change
    add_column :fusion_asks, :status, :boolean
  end
end
