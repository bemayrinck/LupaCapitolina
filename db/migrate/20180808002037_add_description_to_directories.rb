class AddDescriptionToDirectories < ActiveRecord::Migration[5.1]
  def change
    add_column :directories, :description, :text
  end
end
