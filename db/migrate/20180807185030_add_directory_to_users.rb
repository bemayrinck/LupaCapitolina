class AddDirectoryToUsers < ActiveRecord::Migration[5.1]
  def change
    add_reference :users, :directory, foreign_key: true
  end
end
